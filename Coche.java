import javax.swing.JOptionPane; // importamos paquete para que funcione

/** 
 * Una clase para administrar operaciones con Coches: crear, modificar y consultar
 * @version 2, 22/02/18
 * @author Jorge Sell�s
*/

public class Coche {

// declaramos dos variables p�blicas est�ticas necesarias para el control de  los coches fabricados
public static int numCoches = 0; // variable est�tica p�blica de la Clase Coche. Est�tica porque es una variable que depende de la Clase, no de los objetos de la clase. Y p�blica porque se necesita el acceso a su valor desde otras clases
public static final int MAX_COCHES = 5; // variable est�tica final (constante) p�blica de la Clase Coche. Est�tica final porque es una variable que depende de la clase y adem�s tiene un valor fijo que no se puede modificar. P�blica porque se necesita que su valor sea alcanzable desde otra clase

// declaramos privadas las variables de instancia para cumplir con la 1� regla de la Encapsulaci�n
  private String matricula;
  private String marca;
  private String modelo;
  private String color;
  private boolean techoSolar;
  private double kilometros;
  private int numPuertas;
  private int numPlazas;
  
/**
 * M�todo constructor Coche sin par�metros, encargado de inicializar variables
*/

  public Coche() { // inicializamos variables en el constructor
    numCoches++;
    setMatricula("");
    setMarca("Seat");
    setModelo("Altea");
    setColor("Blanco");
    setTechoSolar(false);
    setKilometros(0);
    setNumPuertas(3);
    setNumPlazas(5);
  }
  
/** 
 * M�todo constructor Coche
 * @param matricula El n�mero de matr�cula a asignar al nuevo coche
*/

  public Coche(String matricula) { // sobrecarga de constructor conociendo matr�cula
    this();
    setMatricula(matricula);
  }
  
/** 
 * M�todo constructor Coche
 * @param numPuertas Indica el n�mero de puertas con que el coche va a ser creado
 * @param numPlazas Indica el n�mero de plazas con que el coche va a ser creado
*/

  public Coche(int numPuertas, int numPlazas) { // sobrecarga de constructor conociendo n�mero de puertas y n�mero de plazas
    this();
    setNumPuertas(numPuertas);
    setNumPlazas(numPlazas);
  }
  
/**
 * M�todo constructor Coche
 * @param marca Indica la marca del coche que se va a crear
 * @param modelo Indica el modelo del conche que se va a crear
 * @param color Indica el color del coche que se va a crear
*/
  
  public Coche(String marca, String modelo, String color) { // sobrecarga de constructor conociendo marca, modelo y color
    this();
    setMarca(marca);
    setModelo(modelo);
    setColor(color);
  }
  
/**
 * M�todo para a�adir kil�metros a un coche creado
 * @param kilometros El n�mero de kil�metros que vamos a a�adir a los que tenga
 * @return mensaje de los kil�metros a�adidos en tipo String
*/


  public String avanzar(double kilometros) { // m�todo para a�adir kil�metros al contakil�metros del coche 
    String mensaje;
    if (kilometros>0) {
      setKilometros(getKilometros()+kilometros);
    }
    mensaje="Se han a�adido "+kilometros+" kil�metros. Los kil�metros totales son "+this.getKilometros();
    return mensaje;
  }
   
/** 
 * M�todo para tunear, o sea, poner el contakil�metros a cero e instalar un techo solar
 * @return mensaje de que ha sido tuneado
*/

  public String tunear() { // m�todo para tunear, poner kil�metros a 0 e instalar techo solar si no lo tiene. 
    String mensaje;
    boolean yaTecho = false;
    setKilometros(0);
    if (this.techoSolar) {
      yaTecho=true;
    }
    else {
      setTechoSolar(true);
    }
    if (yaTecho) {
      mensaje="Se ha tuneado el coche. El cuentakil�metros est� a cero y se ha instalado un techo solar";
      return mensaje;
    }
    else {
      mensaje="Se ha tuneado el coche. El cuentakil�metros est� a cero y no se ha instalado un techo solar porque ya estaba instalado";
      return mensaje;
    }
  }

/**
 * M�todo para tunear con par�metro color, pone el contakil�metros a cero, instala un techo solar y pinta el coche del color indicado
 * @param color Color que va a ser pintando el coche
 * @return mensaje de que el coche ha sido tuneado
 * */
  
    public String tunear(String color) { // sobrecarga del m�todo tunear, utilizando como argumento color para asginarlo a la variable de instancia 
    String mensaje = this.tunear();          
    setColor(color);
    return mensaje;
  } 
  
/**
 * M�todo para asignar una matr�cula a un coche
 * @param matricula La matr�cula a asignar al coche*/

  public void setMatricula(String matricula) {
    if (!matricula.equals("")) {
      this.matricula = matricula;
    }
  }
  
/** 
 * M�todo para acceder a la variable de instancia matricula
 * @return Valor de la matr�cula en variable String*/

  public String getMatricula() {
    return this.matricula;
  }
  
/**
 * M�todo para asignar una marca a un coche
 * @param marca La marca a asignar al coche*/


  public void setMarca(String marca) {
    if (!marca.equals("")) {
      this.marca = marca;
     }  
  }

/** 
 * M�todo para acceder a la variable de instancia matricula
 * @return Valor de la marca en variable String*/

  
  public String getMarca() {
    return this.marca;
  }  
  
/**
 * M�todo para asignar un modelo a un coche
 * @param modelo a asignar al coche*/


  public void setModelo(String modelo) {
    if (!modelo.equals("")) {
      this.modelo = modelo;
     }  
  }

/** 
 * M�todo para acceder a la variable de instancia modelo
 * @return Valor del modelo en variable String*/
  
  
  public String getModelo() {
    return this.modelo;
  }
  
/**
 * M�todo para asignar un color a un coche
 * @param color a asignar al coche*/


  public void setColor(String color) {
    if (!color.equals("")) {
      this.color=color;
     }  
  }
  
/** 
 * M�todo para acceder a la variable de instancia color
 * @return Valor del color en variable String*/


  public String getColor() {
    return this.color;
  }

/**
 * M�todo para asignar un techo solar a un coche
 * @param techoSolar true si tiene y false si no tiene techo solar*/


  public void setTechoSolar(boolean techoSolar) {
    this.techoSolar=techoSolar;  
  }
  
  /** 
 * M�todo para acceder a la variable de instancia techoSolar
 * @return true si lo tiene instalado y false si no, en variable boolean */
  
  public boolean getTechoSolar() {
    return this.techoSolar;
  }

/**
 * M�todo para asignar n�mero de kil�metros a un coche
 * @param kilometros Indica n�mero de kil�metros en variable tipo double*/
  
  
  public void setKilometros(double kilometros) {
    if (kilometros>=0) {
      this.kilometros=kilometros;
    }  
  }

/** 
 * M�todo para acceder a la variable de instancia kilometros
 * @return n�mero de kil�metros del coche en varaible double */
   
  public double getKilometros() {
    return this.kilometros;
  }
  
/**
 * M�todo para asignar n�mero de puertas a un coche
 * @param numPuertas Indica n�mero de puertas en variable tipo int*/  
  
  public void setNumPuertas(int numPuertas) {
    if (numPuertas>=1 && numPuertas<=5) {
      this.numPuertas=numPuertas;
    }  
  }
  
/** 
 * M�todo para acceder a la variable de instancia n�mero de puertas del coche 
 * @return n�mero de puerts del coche en variable int */
  
  
  public int getNumPuertas() {
    return this.numPuertas;
  }

/**
 * M�todo para asignar n�mero de plazas a un coche
 * @param numPlazas Indica n�mero de plazas en variable tipo int*/  


  public void setNumPlazas(int numPlazas) {
    if (numPlazas>=1 && numPlazas<=7) {
      this.numPlazas=numPlazas;
    }
  }
  
/** 
 * M�todo para acceder a la variable de instancia n�mero de plazas del coche 
 * @return n�mero de plazas del coche en variable int */


  public int getNumPlazas() {
    return this.numPlazas;
  }
  
}