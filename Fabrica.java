import javax.swing.JOptionPane; // importamos paquete para que funcione

/**
 * Clase para fabricar y adminstrar las caracter�sticas de los coches fabricados
 * @version 2, 22/2/18
 * @author Jorge Sell�s
*/

public class Fabrica {
  
/** 
 * M�todo principal de la clase Fabrica
*/
  
  public static void main(String[] args) {
 
    // variables locales:
    
    String textoMenu = "F�brica de coches de Jorge Sell�s. Selecciona una de las siguientes opciones:"+"\n"+"1. Fabricar coche (conociendo matr�cula)"+"\n"+"2. Fabricar coche (a partir del n� de puertas y el n� de plazas)"+"\n"+"3. Fabricar coche (a partir de la marca, el modelo y el color)"+"\n"+"4. Fabricar coche (sin datos)"+ "\n"+"5. Tunear coche (pintando de un color)"+"\n"+"6. Tunear coche (sin pintarlo)"+"\n"+"7. Avanzar kil�metros"+"\n"+"8. Mostrar caracter�sticas de un coche"+"\n"+"9. Salir del programa"+"\n";
    String opcionMenu = ""; // variable auxiliar para controlar la opci�n elegida
    int numeroOpcion = 0; // variable auxiliar num�rica para controlar la opci�n elegida
    double numeroKilometros = 0;
    int numeroPuertas = 0;
    int numeroPlazas = 0;
    Coche[] cars; // definici�n array de Coche
    cars=new Coche[Coche.MAX_COCHES]; // creaci�n array de n�mero de posiciones igual a n�mero m�ximo de coches a fabricar
    Coche car; // car declarada como objeto Coche
    String matricula;
    int lugarIndice; // auxiliar para saber en qu� posci�n del array est� el coche buscado
  
    while (numeroOpcion!=9) { // mientras la opci�n de men� no sea 9, volver a pedirla con seguridad para no prducir excepci�n
      
      do {
        try { // inicio parte del programa donde puede haber un error, para que quede controlado 
          opcionMenu = JOptionPane.showInputDialog (textoMenu);
          numeroOpcion = Integer.parseInt(opcionMenu);
        }
        catch (NumberFormatException e) { // si hay un error, este m�todo coger� el control  
          JOptionPane.showMessageDialog (null, "Por favor, introduzca una opci�n correcta");// mostrar� este mensaje
        }
      }
      while (numeroOpcion<1 || numeroOpcion>9); // mientras la opci�n de men� no est� entre 0 y 9, volver a pedirla
        
      switch (numeroOpcion) { // la opci�n de men� est� entre 1 y 9, e indicamos el c�digo de cada opci�n en cada case correspondiente.
      
        case 1: // fabricar coche conociendo matr�cula
      
          if (Coche.numCoches<Coche.MAX_COCHES){ // comprobar que para fabricar este coche, no est� llena la f�brica
          
            matricula = JOptionPane.showInputDialog ("Introduce la matr�cula del Coche: ");
            car = new Coche(matricula); // llamada a constructor conociendo matr�cula
        
            // pedimos resto de datos:
        
            String marca = JOptionPane.showInputDialog ("Introduce la marca del Coche: ");
            car.setMarca(marca);
            String modelo = JOptionPane.showInputDialog ("Introduce el modelo del Coche: ");
            car.setModelo(modelo);
            String color = JOptionPane.showInputDialog ("Introduce el color del Coche: ");
            car.setColor(color);
    
            // datos num�ricos, solicitados con seguridad para no producir excepci�n:
    
            try { // inicio parte del programa donde puede haber un error, para que quede controlado 
              String kilometros = JOptionPane.showInputDialog ("Introduce el n�mero de kil�metros del Coche: ");
              numeroKilometros = Double.parseDouble(kilometros);
              car.setKilometros(numeroKilometros);
            }
            catch (NumberFormatException e) { // si hay un error, este m�todo coger� el control  
              JOptionPane.showMessageDialog (null, "Dato incorrecto, se inicializar� al valor correspondiente");// mostrar� este mensaje
              car.setKilometros(0);
            }
            try { // inicio parte del programa donde puede haber un error, para que quede controlado 
              String numPuertas = JOptionPane.showInputDialog ("Introduce el n�mero de puertas del Coche: ");
              numeroPuertas = Integer.parseInt(numPuertas);
              car.setNumPuertas(numeroPuertas);
            }
            catch (NumberFormatException e) { // si hay un error, este m�todo coger� el control  
              JOptionPane.showMessageDialog (null, "Dato incorrecto, se inicializar� al valor correspondiente");// mostrar� este mensaje
              car.setNumPuertas(3);
            }
            try { // inicio parte del programa donde puede haber un error, para que quede controlado 
              String numPlazas = JOptionPane.showInputDialog ("Introduce el n�mero de plazas del Coche: ");
              numeroPlazas = Integer.parseInt(numPlazas);
              car.setNumPlazas(numeroPlazas);  
            }
            catch (NumberFormatException e) { // si hay un error, este m�todo coger� el control  
              JOptionPane.showMessageDialog (null, "Dato incorrecto, se inicializar� al valor correspondiente");// mostrar� este mensaje
              car.setNumPlazas(5);  
            }
            String techoSolar = JOptionPane.showInputDialog ("Introduce si el Coche dispone de techo solar (S/N): ");
            if (techoSolar.equals("s") || techoSolar.equals("S")) {
              car.setTechoSolar(true);
            }
            else if (techoSolar.equals("n") || techoSolar.equals("N")) {
              car.setTechoSolar(false);
            }
            else {
              JOptionPane.showMessageDialog (null, "Dato incorrecto, se inicializar� al valor por defecto");// mostrar� este mensaje
              car.setTechoSolar(false);    
            }
            cars[Coche.numCoches-1]=car;
          }
          else {
            JOptionPane.showMessageDialog (null, "No se puede crear un coche nuevo porque la f�brica est� llena");// mostrar� este mensaje
            break;  
          }
          JOptionPane.showMessageDialog (null, caracteristicas(car)); // muestra las caracter�sticas del coche fabricado
          break;
      
        case 2: // fabricar coche conociendo n�mero de puertas y n�mero de plazas
        
          if (Coche.numCoches<Coche.MAX_COCHES){ // comprobar que para fabricar coche no est� llena la f�brica
          
            // datos num�ricos, solicitados con seguridad para no producir excepci�n:
           
            try { // inicio parte del programa donde puede haber un error, para que quede controlado 
              String numPuertas = JOptionPane.showInputDialog ("Introduce el n�mero de puertas del Coche: ");
              numeroPuertas = Integer.parseInt(numPuertas);
            }
            catch (NumberFormatException e) { // si hay un error, este m�todo coger� el control  
              JOptionPane.showMessageDialog (null, "Dato incorrecto, se inicializar� al valor correspondiente");// mostrar� este mensaje
              numeroPuertas=3;
            }
            try { // inicio parte del programa donde puede haber un error, para que quede controlado 
              String numPlazas = JOptionPane.showInputDialog ("Introduce el n�mero de plazas del Coche: ");
              numeroPlazas = Integer.parseInt(numPlazas);
            }
            catch (NumberFormatException e) { // si hay un error, este m�todo coger� el control  
              JOptionPane.showMessageDialog (null, "Dato incorrecto, se inicializar� al valor correspondiente");// mostrar� este mensaje
              numeroPlazas=5;  
            }
          
            car = new Coche(numeroPuertas, numeroPlazas);  // llamada a constructor conociendo n�mero de puertas y n�mero de plazas
          
            // conseguimos el resto de datos necesarios:
            
            matricula = matAleatoria(); // llamamos a m�todo para que nos cree una matr�cula aleatoria
            car.setMatricula(matricula);
            String marca = JOptionPane.showInputDialog ("Introduce la marca del Coche: ");
            car.setMarca(marca);
            String modelo = JOptionPane.showInputDialog ("Introduce el modelo del Coche: ");
            car.setModelo(modelo);
            String color = JOptionPane.showInputDialog ("Introduce el color del Coche: ");
            car.setColor(color);
          
            try { // inicio parte del programa donde puede haber un error, para que quede controlado 
              String kilometros = JOptionPane.showInputDialog ("Introduce el n�mero de kil�metros del Coche: ");
              numeroKilometros = Double.parseDouble(kilometros);
              car.setKilometros(numeroKilometros);
            }
            catch (NumberFormatException e) { // si hay un error, este m�todo coger� el control  
              JOptionPane.showMessageDialog (null, "Dato incorrecto, se inicializar� al valor correspondiente");// mostrar� este mensaje
              car.setKilometros(0);
            }
            String techoSolar = JOptionPane.showInputDialog ("Introduce si el Coche dispone de techo solar (S/N): ");
            if (techoSolar.equals("s") || techoSolar.equals("S")) {
              car.setTechoSolar(true);
            }
            else if (techoSolar.equals("n") || techoSolar.equals("N")) {
              car.setTechoSolar(false);
            }
            else {
              JOptionPane.showMessageDialog (null, "Dato incorrecto, se inicializar� al valor por defecto");// mostrar� este mensaje
              car.setTechoSolar(false);
             }    
            cars[Coche.numCoches-1]=car;        
          }
          else {
            JOptionPane.showMessageDialog (null, "No se puede crear un coche nuevo porque el garage est� lleno");// mostrar� este mensaje
            break;  
          }
          JOptionPane.showMessageDialog (null, caracteristicas(car)); // mostramos datos del coche fabricado
          break;        
         
        case 3: // fabricar coche conociendo la marca, el modelo y el color
      
          if (Coche.numCoches<Coche.MAX_COCHES){ // comprobar que para fabricar coche no est� llena la f�brica
          
            String marca = JOptionPane.showInputDialog ("Introduce la marca del Coche: ");
            String modelo = JOptionPane.showInputDialog ("Introduce el modelo del Coche: ");
            String color = JOptionPane.showInputDialog ("Introduce el color del Coche: ");
                    
            car = new Coche(marca, modelo, color);  // llamada a constructor conociendo marca, modelo y color
          
            matricula = matAleatoria(); // llamada a m�todo para que nos genere una matr�cula nueva
            car.setMatricula(matricula);
          
              // datos num�ricos, solicitados con seguridad para no producir excepci�n:

            try { // inicio parte del programa donde puede haber un error, para que quede controlado 
              String kilometros = JOptionPane.showInputDialog ("Introduce el n�mero de kil�metros del Coche: ");
              numeroKilometros = Double.parseDouble(kilometros);
              car.setKilometros(numeroKilometros);
            }
            catch (NumberFormatException e) { // si hay un error, este m�todo coger� el control  
              JOptionPane.showMessageDialog (null, "Dato incorrecto, se inicializar� al valor correspondiente");// mostrar� este mensaje
              car.setKilometros(0);
            }
            try { // inicio parte del programa donde puede haber un error, para que quede controlado 
              String numPuertas = JOptionPane.showInputDialog ("Introduce el n�mero de puertas del Coche: ");
              numeroPuertas = Integer.parseInt(numPuertas);
              car.setNumPuertas(numeroPuertas);
            }
            catch (NumberFormatException e) { // si hay un error, este m�todo coger� el control  
              JOptionPane.showMessageDialog (null, "Dato incorrecto, se inicializar� al valor correspondiente");// mostrar� este mensaje
              car.setNumPuertas(3);
            }
            try { // inicio parte del programa donde puede haber un error, para que quede controlado 
              String numPlazas = JOptionPane.showInputDialog ("Introduce el n�mero de plazas del Coche: ");
              numeroPlazas = Integer.parseInt(numPlazas);
              car.setNumPlazas(numeroPlazas);  
            }
            catch (NumberFormatException e) { // si hay un error, este m�todo coger� el control  
              JOptionPane.showMessageDialog (null, "Dato incorrecto, se inicializar� al valor correspondiente");// mostrar� este mensaje
              car.setNumPlazas(5);  
            }
          
            String techoSolar = JOptionPane.showInputDialog ("Introduce si el Coche dispone de techo solar (S/N): ");
            if (techoSolar.equals("s") || techoSolar.equals("S")) {
              car.setTechoSolar(true);
            }
            else if (techoSolar.equals("n") || techoSolar.equals("N")) {
              car.setTechoSolar(false);
            }
            else {
              JOptionPane.showMessageDialog (null, "Dato incorrecto, se inicializar� al valor por defecto");// mostrar� este mensaje
              car.setTechoSolar(false);
             }    
            cars[Coche.numCoches-1]=car;        
          }
          else {
            JOptionPane.showMessageDialog (null, "No se puede crear un coche nuevo porque el garage est� lleno");// mostrar� este mensaje
            break;  
          }
          JOptionPane.showMessageDialog (null, caracteristicas(car)); // muestra los datos del coche fabricado
          break;
      
        case 4: // fabricar coche sin conocer ning�n dato
      
          if (Coche.numCoches<Coche.MAX_COCHES){
          
            car = new Coche(); // llamada a constructor de coches sin conocer ning�n dato
            matricula = matAleatoria(); // llamada a m�todo paraque nos cree una matr�cula nueva
            car.setMatricula(matricula);
          
            // pedimos el resto de datos:
          
            String marca = JOptionPane.showInputDialog ("Introduce la marca del Coche: ");
            car.setMarca(marca);
            String modelo = JOptionPane.showInputDialog ("Introduce el modelo del Coche: ");
            car.setModelo(modelo);
            String color = JOptionPane.showInputDialog ("Introduce el color del Coche: ");
            car.setColor(color);

            // pedimos datos num�ricos con seguridad
            
            try { // inicio parte del programa donde puede haber un error, para que quede controlado 
              String kilometros = JOptionPane.showInputDialog ("Introduce el n�mero de kil�metros del Coche: ");
              numeroKilometros = Double.parseDouble(kilometros);
              car.setKilometros(numeroKilometros);
            }
            catch (NumberFormatException e) { // si hay un error, este m�todo coger� el control  
              JOptionPane.showMessageDialog (null, "Dato incorrecto, se inicializar� al valor correspondiente");// mostrar� este mensaje
              car.setKilometros(0);
            }
            try { // inicio parte del programa donde puede haber un error, para que quede controlado 
              String numPuertas = JOptionPane.showInputDialog ("Introduce el n�mero de puertas del Coche: ");
              numeroPuertas = Integer.parseInt(numPuertas);
              car.setNumPuertas(numeroPuertas);
            }
            catch (NumberFormatException e) { // si hay un error, este m�todo coger� el control  
              JOptionPane.showMessageDialog (null, "Dato incorrecto, se inicializar� al valor correspondiente");// mostrar� este mensaje
              car.setNumPuertas(3);
            }
            try { // inicio parte del programa donde puede haber un error, para que quede controlado 
              String numPlazas = JOptionPane.showInputDialog ("Introduce el n�mero de plazas del Coche: ");
              numeroPlazas = Integer.parseInt(numPlazas);
              car.setNumPlazas(numeroPlazas);  
            }
            catch (NumberFormatException e) { // si hay un error, este m�todo coger� el control  
              JOptionPane.showMessageDialog (null, "Dato incorrecto, se inicializar� al valor correspondiente");// mostrar� este mensaje
              car.setNumPlazas(5);  
            }
          
            String techoSolar = JOptionPane.showInputDialog ("Introduce si el Coche dispone de techo solar (S/N): ");
            if (techoSolar.equals("s") || techoSolar.equals("S")) {
              car.setTechoSolar(true);
            }
            else if (techoSolar.equals("n") || techoSolar.equals("N")) {
              car.setTechoSolar(false);
            }
            else {
              JOptionPane.showMessageDialog (null, "Dato incorrecto, se inicializar� al valor por defecto");// mostrar� este mensaje
              car.setTechoSolar(false);
            }    
            cars[Coche.numCoches-1]=car;        
          }
          else {
            JOptionPane.showMessageDialog (null, "No se puede crear un coche nuevo porque el garage est� lleno");// mostrar� este mensaje
            break;  
          }
          JOptionPane.showMessageDialog (null, caracteristicas(car)); // mostramos la caracter�sticas del coche fabricado
          break;
        
        case 5: // Tunear coche pintandolo de color
      
          // pedir matr�cula, pedir color, buscar coche y tunear(color)
        
          matricula = JOptionPane.showInputDialog ("Introduce la matr�cula del Coche: "); 
          String color = JOptionPane.showInputDialog ("Introduce el color del Coche: ");
          lugarIndice=buscaCoche(cars, matricula); // enviamos Array de coches y n�mero de matr�cula para que el m�todo nos localice el lugar del array en el que est� el coche
          if (lugarIndice==-1) {
            JOptionPane.showMessageDialog (null, "El coche no se ha localizado, int�ntelo de nuevo");// mostrar� mensaje de que el coche ha sido tuneado
            break;
          } 
          else { 
            String mensajeTunear=cars[lugarIndice].tunear(color); // recibimos mensaje del m�todo tunear con color, discriminando si se ha instalado el techo solar, o si ya estaba
            JOptionPane.showMessageDialog (null, mensajeTunear);// mostrar� mensaje de que el coche ha sido tuneado 
            JOptionPane.showMessageDialog (null, caracteristicas(cars[lugarIndice])); // mostramos datos del coche fabricado
          }
          break;
        
        case 6: // tuenar coche sin pintarlo de color
      
          // pedir matr�cula, buscar coche y tunear() 
      
          matricula = JOptionPane.showInputDialog ("Introduce la matr�cula del Coche: ");
          lugarIndice=buscaCoche(cars, matricula);
          if (lugarIndice==-1) {
            JOptionPane.showMessageDialog (null, "El coche no se ha localizado, int�ntelo de nuevo");// mostrar� mensaje de que el coche ha sido tuneado
            break;
          }  
          else {  
            String mensajeTuenar=cars[lugarIndice].tunear();
            JOptionPane.showMessageDialog (null, mensajeTuenar);// mostrar� mensaje de que el coche ha sido tuneado
            JOptionPane.showMessageDialog (null, caracteristicas(cars[lugarIndice])); // mostramos datos del coche fabricado
          }
          break;
          
      
        case 7: // Avanzar kil�metros 
        
          // pedir matr�cula, pedir kil�metros, buscar coche y avanzar(kilometros)
        
          matricula = JOptionPane.showInputDialog ("Introduce la matr�cula del Coche: ");
          try { // inicio parte del programa donde puede haber un error, para que quede controlado 
            String kilometros = JOptionPane.showInputDialog ("Introduce el n�mero de kil�metros del Coche: ");
            numeroKilometros = Double.parseDouble(kilometros);
          }
          catch (NumberFormatException e) { // si hay un error, este m�todo coger� el control  
            JOptionPane.showMessageDialog (null, "Dato incorrecto, se inicializar� al valor correspondiente");// mostrar� este mensaje
            numeroKilometros = 0;
          }
          lugarIndice=buscaCoche(cars, matricula);
          if (lugarIndice==-1) {
            JOptionPane.showMessageDialog (null, "El coche no se ha localizado, int�ntelo de nuevo");// mostrar� mensaje de que el coche ha sido tuneado
            break;
          } 
          else {
            String mensajeAvanzar=cars[lugarIndice].avanzar(numeroKilometros);
            JOptionPane.showMessageDialog (null, mensajeAvanzar);// mostrar� este mensaje
            JOptionPane.showMessageDialog (null, caracteristicas(cars[lugarIndice])); // mostramos datos del coche fabricado
          }
          break;
                
        case 8: // mostrar datos de un coche
              
          // pedir matr�cula y mostrar caracter�sticas del coche.
      
          matricula = JOptionPane.showInputDialog ("Introduce la matr�cula del Coche: ");
          lugarIndice=buscaCoche(cars, matricula);
          if (lugarIndice==-1) {
            JOptionPane.showMessageDialog (null, "El coche no se ha localizado, int�ntelo de nuevo");// mostrar� mensaje de que el coche ha sido tuneado
            break;
          }
          else {
          caracteristicas(cars[lugarIndice]);
          JOptionPane.showMessageDialog (null, caracteristicas(cars[lugarIndice])); // mostramos datos del coche fabricado 
          }
          break;
        
        case 9: // salir del programa
        
          break;
    }
  }  
}
  
/** 
 * M�todo que calcula una matr�cula aleatoria de un coche compuesta por 5 n�meros entre 0 y 9
 * @return valor de la matr�cula en variable String
*/

  
public static String matAleatoria() {  // genera una matr�cula aleatoria
    int numero;
    String numeroMatriculaAleatoria="";
    for (int i=0; i<5; i++) {
      numero = (int) (Math.random()*10);
      numeroMatriculaAleatoria += numero;
    }
    return numeroMatriculaAleatoria;
}
    
/** 
 * M�todo que busca un coche por su n�mero de matr�cula, dentro de un array 
 * @param cars[] de tipo Coche, es el array que puede contener el coche a buscar por matr�cula
 * @param matricula Es el valor a buscar en el array para localizar el coche
 * @return Devuelve la posici�n el array en el que se encuentra el coche o -1 si no lo encuentra
*/

public static int buscaCoche(Coche cars[], String matricula) {  // busca un coche de una matr�cula concreta dentro de una array y devuelve la posici�n �ndice donde se encuentra en el array
  
     for (int i=0; i<Coche.numCoches; i++) {
        if ((cars[i].getMatricula()).equals(matricula)) {
          return i;
        }
     }
  return -1;
}
   
/** 
 * M�todo que muestra las caracter�sticas de un coche 
 * @param c El Coche del que tenemos que mostrar los datos tipo Coche
 * @return varialbe tipo String con el mensaje que contiene todos los datos del coche
*/

public static String caracteristicas(Coche c) { // muestra las caracter�sticas de un coche 
    String mensaje;
    String mensajeTechoSolar;
    if (c.getTechoSolar()) {
      mensajeTechoSolar ="y tiene techo solar";
    }
    else {
      mensajeTechoSolar = "y no tiene techo solar";
    }
    mensaje="El coche tiene los siguientes datos: "+"\n"+"Matr�cula: "+c.getMatricula()+"\n"+"Marca: "+c.getMarca()+"\n"+"Modelo: "+c.getModelo()+"\n"+"Color: "+c.getColor()+"\n"+"Kil�metros: "+c.getKilometros()+"\n"+mensajeTechoSolar+"\n"+"Tiene "+c.getNumPuertas()+" puertas y "+c.getNumPlazas()+" plazas";
    return mensaje;
} 
}